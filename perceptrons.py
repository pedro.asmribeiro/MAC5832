
# coding: utf-8

# # Classical perceptron and pocket perceptron functions

# In[ ]:


import numpy as np


# In[2]:


def perceptron(x,y,w0):
    x=np.c_[np.ones(len(x)),x] #add x0=[1...1], the offset constant
    w=w0
    e_in=np.zeros(0)
    change=True
    j=0
    while change!=False:
        y_prime=np.sign(np.matmul(x,w))
        e_in=np.append(e_in,np.mean(np.abs(y_prime-y)))
        for i in range(len(y)):
            if y_prime[i]!=y[i]:
                w=w+y[i]*x[i]
                change=True
                break
            else:
                change=False
        j+=1
        print(j,change,w,y_prime,y)
    return [j,w,y_prime,e_in]


# In[3]:


def pocket(x,y,w0,stop):
    x=np.c_[np.ones(len(x)),x] #add x0=[1...1], the offset constant
    w=w0
    e_in=np.zeros(0) #compute in-sample error
    e_in_pocket=np.inf
    change=True
    for j in range(stop): #compute until stop criteria or convergence, whatever happens first
        if change==False:
            break #detecting convergence, if possible
        y_prime=np.sign(np.matmul(x,w))
        e_in_now=np.mean(np.abs(y_prime-y))
        e_in=np.append(e_in,e_in_pocket)
        if e_in_now<e_in_pocket:
            e_in_pocket=e_in_now
            w_pocket=w #putting w in my "pocket"
        for i in range(len(y)):
            if y_prime[i]!=y[i]:
                w=w+y[i]*x[i]
                change=True #has not converged
                break
            else:
                change=False
        #print(j,change,w,y_prime,y,e_in)
    return [j,w_pocket,y_prime,e_in,e_in_pocket]

