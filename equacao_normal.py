#NUSP 8011914

import numpy as np

def normal_equation_prediction(X, y):
    """
    Calculates the prediction using the normal equation method.
    You should add a new column with 1s.

    :param X: design matrix
    :type X: np.array
    :param y: regression targets
    :type y: np.array
    :return: prediction
    :rtype: np.array
    """
    # complete a função e remova a linha abaixo
    
    prediction=np.zeros(len(X))
    x=np.c_[np.ones(len(X),dtype=np.int8),X] # add 1 collum (FORTRAN style)
    w=np.linalg.inv(x.T.dot(x)).dot(x.T).dot(y)
    for i in range(len(x)):
        prediction[i]=w.T.dot(x[i])