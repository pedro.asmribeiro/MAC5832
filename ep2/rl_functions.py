import numpy as np
from util import randomize_in_place


def linear_regression_prediction(X, w):
    """
    Calculates the linear regression prediction.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param w: weights
    :type w: np.array(shape=(d, 1))
    :return: prediction
    :rtype: np.array(shape=(N, 1))
    """

    return X.dot(w)


def standardize(X):
    """
    Returns standardized version of the ndarray 'X'.

    :param X: input array
    :type X: np.ndarray(shape=(N, d))
    :return: standardized array
    :rtype: np.ndarray(shape=(N, d))
    """

    # YOUR CODE HERE:
    X_out=(X-X.mean(axis=0))/X.std(axis=0)
    #raise NotImplementedError
    # END YOUR CODE

    return X_out


def compute_cost(X, y, w):
    """
    Calculates  mean square error cost.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param w: weights
    :type w: np.array(shape=(d,))
    :return: cost
    :rtype: float
    """

    # YOUR CODE HERE:
    N=np.shape(X)[0]
    J=np.asscalar((X.dot(w)-y).T.dot(X.dot(w)-y)/N)
    #raise NotImplementedError
    # END YOUR CODE

    return J


def compute_wgrad(X, y, w):
    """
    Calculates gradient of J(w) with respect to w.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param w: weights
    :type w: np.array(shape=(d,))
    :return: gradient
    :rtype: np.array(shape=(d,))
    """

    # YOUR CODE HERE:
    N,d=np.shape(X)
    grad=np.zeros(d)
    for j in range(d):
        grad[j]=(2./N)*sum([(np.dot(X[i],w)-y[i])*X[i,j] for i in range(N)])
    #raise NotImplementedError
    # END YOUR CODE

    return grad


def batch_gradient_descent(X, y, w, learning_rate, num_iters):
    """
     Performs batch gradient descent optimization.

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param w: weights
    :type w: np.array(shape=(d,))
    :param learning_rate: learning rate
    :type learning_rate: float
    :param num_iters: number of iterations
    :type num_iters: int
    :return: weights, weights history, cost history
    :rtype: np.array(shape=(d,)), list, list
    """

    weights_history = [w]
    cost_history = [compute_cost(X, y, w)]

    # YOUR CODE HERE:
    J=cost_history[0]
    for t in range(num_iters):
        grad=compute_wgrad(X,y,w)
        w=(w.T-learning_rate*grad).T
        weights_history.append(w.flatten())
        J_new=compute_cost(X,y,w)
        cost_history.append(J_new)
        if (J_new-J)==0:
            break
        J=J_new
    #raise NotImplementedError
    # END YOUR CODE

    return w, weights_history, cost_history


def stochastic_gradient_descent(X, y, w, learning_rate, num_iters, batch_size):
    """
     Performs stochastic gradient descent optimization

    :param X: design matrix
    :type X: np.ndarray(shape=(N, d))
    :param y: regression targets
    :type y: np.ndarray(shape=(N, 1))
    :param w: weights
    :type w: np.array(shape=(d, 1))
    :param learning_rate: learning rate
    :type learning_rate: float
    :param num_iters: number of iterations
    :type num_iters: int
    :param batch_size: size of the minibatch
    :type batch_size: int
    :return: weights, weights history, cost history
    :rtype: np.array(shape=(d, 1)), list, list
    """
    # YOUR CODE HERE:
    weights_history = [w]
    cost_history = [compute_cost(X, y, w)]
    J=cost_history[0]
    for t in range(num_iters):
        indices=np.random.randint(0,len(y),size=batch_size)
        X_rand=X[indices]
        y_rand=y[indices]
        grad=compute_wgrad(X_rand,y_rand,w)
        w=(w.T-learning_rate*grad).T
        weights_history.append(w.flatten())
        J_new=compute_cost(X,y,w)
        cost_history.append(J_new)
        if (J_new-J)==0:
            break
        J=J_new
    #raise NotImplementedError
    # END YOUR CODE

    return w, weights_history, cost_history
