#!/bin/sh
docker run -v $(pwd):/home/notebooks -p 8888:8888 intelpython/intelpython3_full jupyter notebook --ip='*' --port=8888 --allow-root --no-browser 
